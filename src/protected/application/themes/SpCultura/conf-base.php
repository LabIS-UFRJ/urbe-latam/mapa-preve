<?php

#ADD MAPA-PREVE
return [
//    // latitude,longitude do centro do mapa da busca e do mapa da criação de agentes e espaços
     'maps.center' => [-22.93577, -43.09840],
//
//    // a cada quantos pixels se deve criar um cluster no mapa da busca
     'maps.maxClusterRadius' => 20,
//
//    // simplificação do shapefile
//    'maps.geometryFieldQuery' => "ST_SimplifyPreserveTopology(geom, 0.001)",
//
//    // zoom padrão do mapa da busca
     'maps.zoom.default' => 18,
//
//    // zoom do mapa da single do agente quando este define a posição como aproximada
     'maps.zoom.approximate' => 30,
//
//    // zoom do mapa da single do agente quando este define a posição como precisa
     'maps.zoom.precise' => 50,
//
//    // zoom máximo do mapa da busca
     'maps.zoom.max' => 50,
//
//    // zom mínimo do mapa da busca
     'maps.zoom.min' => 5,
//    // incluir no mapa as camadas do google?
//    'maps.includeGoogleLayers' => false,
//    // descomente para definir quais fivisões geográficas são utilizadas no tema
//    // devem ser as mesmas dos shapefiles
//    'app.geoDivisionsHierarchy' => [
//        'pais'          => 'País',
//        'regiao'        => 'Região',
//        'estado'        => 'Estado',
//        'mesorregiao'   => 'Mesorregião',
//        'microrregiao'  => 'Microrregião',
         'municipio'     => 'Niterói',
//        'zona'          => 'Zona',
//        'subprefeitura' => 'Subprefeitura',
//        'distrito'      => 'Distrito'
//    ],
];
#END MAPA-PREVE
